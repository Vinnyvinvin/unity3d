# Optimization ... in 3D!
## A "Sound Advice" module

Copyright (C) Kevin Cole 2018 CC-BY-SA

This is intended to be a 3D version of the 2D Optimization module.
Instead of [Python](https://www.python.org/) and
[Qt](https://www.qt.io/) as the tools of choice, this uses [Unity
3D}(https://unity3d.com/) and
[C#](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/)
to get the job done.

It would be preferable to use an open source engine... So, I'm 
"waiting for [Godot](https://godotengine.org/)".

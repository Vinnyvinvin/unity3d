﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InputScript : MonoBehaviour {

    void Start()
    {
        

        GameObject inpScript = GameObject.Find("InputField");
        var input = inpScript.GetComponent<InputField>();
        var se = new InputField.SubmitEvent();

        //se.AddListener(SubmitName);
        input.onEndEdit = se;
    }

    private void SubmitRoomSize(string arg0)
    {
        GameObject sg = GameObject.Find("Sound Sources");
        SurfaceGenerator sgScript = sg.GetComponent<SurfaceGenerator>();
        sgScript.floorSize = float.Parse(arg0);
        //Destroy(input);
        //Debug.Log(arg0);

    }
}

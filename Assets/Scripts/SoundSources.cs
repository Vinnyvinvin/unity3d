//
//  Source: SoundSources.cs
//  Author: Kevin Cole <kevin.cole@novawebdevelopment.org>
//          Sophia Wallace <sophiawallace019@gmail.com>
//
//  Creates 8 instances of a prefab arranged in a circle
//
//  See:
//    https://docs.unity3d.com/Manual/InstantiatingPrefabs.html
//
//  Copyright (c) 2017 Kevin Cole CC-BY-SA (C) 2017
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSources : MonoBehaviour {
    public  Transform source;
    private Transform x;
    
    private int numberOfObjects = 8;
    private float radius;

    Signal[] myObjects = new Signal[8];
    public int signalDelay;
    public  OSC oscReference;
    private OscMessage message;
    private string objName;


    void Start() {
        GameObject sg = GameObject.Find("Sound Sources");
        SurfaceGenerator sgScript = sg.GetComponent<SurfaceGenerator>();
        radius = 4f * sgScript.floorSize;
        //Debug.Log("Radius: " + radius);


        message = new OscMessage();
        message.address = "/osc/respond_to";
        message.values.Add(8001);
        oscReference.Send(message);
        System.Threading.Thread.Sleep(50);
        message = new OscMessage();
        message.address = "/osc/notify/vcs/SoundAdvice";
        message.values.Add(1);
        oscReference.Send(message);
        System.Threading.Thread.Sleep(50);
        for (int i = 0; i < numberOfObjects; i++) {
            float angle = (i * Mathf.PI * 2 / numberOfObjects) + (Mathf.PI / 2.0f);
            Vector3 pos = new Vector3(Mathf.Cos(angle) * radius, 0.5f, Mathf.Sin(angle) * radius);
            //x = Instantiate(source, pos, Quaternion.identity);
            //x.name = "Signal" + (i + 1).ToString();
            myObjects[i] = new Signal("Signal" + (i+1).ToString(), pos);
            //Debug.Log("Signal" + (i + 1).ToString() + " made");


            message = new OscMessage();
            //message.address = "/vcs/" + x.name + "/1";
            message.address = "/vcs/" + myObjects[i].getName() + "/1";
            message.values.Add(1.0f);
            oscReference.Send(message);
            System.Threading.Thread.Sleep(50);
            //Debug.Log(myObjects[i].getName());
        }
        
        message = new OscMessage();
        message.address = "/vcs/Replay/1";
        message.values.Add(1.0f);
        oscReference.Send(message);
        System.Threading.Thread.Sleep(50);
    }
    void LateUpdate()
    {
        GameObject player = GameObject.Find("Player");
        if (!player.GetComponent<Rigidbody>().IsSleeping())
        {
            for (int i = 0; i < 8; i++)
            {
                myObjects[i].updateValues(radius);

                message = new OscMessage();
                message.address = "/vcs/dBSignal" + (i + 1).ToString() + "/1";
                message.values.Add(myObjects[i].getAttenuation());
                oscReference.Send(message);

                Debug.Log("Signal " + (i + 1) + " attenuation: " + myObjects[i].getAttenuation());
                Debug.Log("Signal " + (i + 1) + " Kyma conversion: " + myObjects[i].getKymaConversion());
                /*
                if (i == 8)
                {
                    System.Threading.Thread.Sleep(signalDelay);
                    i = 0;
                }
                */
            }
        }

    }
}

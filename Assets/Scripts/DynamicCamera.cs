﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicCamera : MonoBehaviour {
    //Written by Sophia Wallace <sophiawallace019@gmail.com>

    void Start ()
    {
        GameObject sg = GameObject.Find("Sound Sources");
        SurfaceGenerator sgScript = sg.GetComponent<SurfaceGenerator>();

        transform.position = new Vector3(0, sgScript.floorSize * 10, 0); //-0.05f
    }
}

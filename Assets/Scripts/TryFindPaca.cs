// Copied by Kevin Cole <kevin.cole@novawebdevelopment.org> 2017.11.22
// Source: https://docs.unity3d.com/ScriptReference/Networking.NetworkDiscovery.html
//
//
// Attempting to get zeroconf / avahi / bonjour / rendezvous / mDNS to work.
//

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class OverriddenNetworkDiscovery : NetworkDiscovery
{
    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        NetworkManager.singleton.networkAddress = fromAddress;
        NetworkManager.singleton.StartClient();
    }
}

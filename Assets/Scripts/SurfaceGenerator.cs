﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceGenerator : MonoBehaviour {
    //Written by Sophia Wallace <sophiawallace019@gmail.com>

    //All sizes here should be dynamic
    public float floorSize; //floor is a square shape

	void Start ()
    {
        //Idea: create ArrayList to loop through walls instead.

        floorSize = Mathf.Abs(floorSize);
        if(floorSize == 0)
        {
            floorSize = 2f;
        }

        var floor = GameObject.CreatePrimitive(PrimitiveType.Plane);
        floor.name = "Floor";
        floor.GetComponent<Renderer>().material.color = Color.blue;
        floor.transform.localScale = new Vector3(floorSize, 1.0f, floorSize); 
        
        

        var wallN = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wallN.name = "North Wall";
        wallN.transform.localScale = new Vector3(floorSize * 10, 2.0f, 0.25f * floorSize);
        wallN.transform.position = new Vector3(0,0, floorSize * 5);

        var wallE = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wallE.name = "East Wall";
        wallE.transform.localScale = new Vector3(floorSize * 0.25f, 2.0f, 10 * floorSize);
        wallE.transform.position = new Vector3(floorSize * 5, 0, 0);

        var wallS = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wallS.name = "South Wall";
        wallS.transform.localScale = new Vector3(floorSize * 10, 2.0f, 0.25f * floorSize);
        wallS.transform.position = new Vector3(0, 0, floorSize * -5);

        var wallW = GameObject.CreatePrimitive(PrimitiveType.Cube);
        wallW.name = "East Wall";
        wallW.transform.localScale = new Vector3(floorSize * 0.25f, 2.0f, 10 * floorSize);
        wallW.transform.position = new Vector3(floorSize * -5, 0, 0);


    }

}

﻿
using UnityEngine;

internal class Signal
{
    //Written by Sophia Wallace <sophiawallace019@gmail.com>
    private Vector3 pos;
    private AudioClip sound;
    private GameObject player;
    private GameObject tempName;
    private string name;

    //Variables needed to calculate and convert attenuation
    //private float radius;
    private float attenuation;
    private float kymaConversion;
    

    //Used to determine if message should be sent to Kyma (if it's stationary, why bother?)
    private float currentDistance;

    public Signal(string name, AudioClip sound, Vector3 pos) //final constructor
    {

        //tempName = GameObject.Instantiate(GameObject.CreatePrimitive(PrimitiveType.Cube), pos, Quaternion.identity);
        tempName = GameObject.CreatePrimitive(PrimitiveType.Cube);
        tempName.transform.position = pos;
        tempName.transform.rotation = Quaternion.identity;
        tempName.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        //tempName.tag = "Pick Up"; //important, makes sure that player can pick it up
        tempName.GetComponent<Renderer>().material.color = Color.yellow;
        this.pos = pos;
        //AUDIO SOURCE HERE
        AudioSource soundSource = tempName.AddComponent<AudioSource>();


        //copying components of Pick Up prefab
        Rigidbody rb = tempName.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = true;
        BoxCollider bc = tempName.AddComponent<BoxCollider>();
        Rotate rotator = tempName.AddComponent<Rotate>();
        bc.isTrigger = true;
        bc.size = new Vector3(1, 1, 1);

        this.name = name; //Changing name to Signal(1-8) by loop in SoundSources
    }

    public Signal(string name, Vector3 pos) //Used for testing without sound
    {
        //tempName = GameObject.Instantiate(GameObject.CreatePrimitive(PrimitiveType.Cube), pos, Quaternion.identity);
        tempName = GameObject.CreatePrimitive(PrimitiveType.Cube);
        tempName.transform.position = pos;
        tempName.transform.rotation = Quaternion.identity;
        tempName.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        //tempName.tag = "Pick Up";
        tempName.GetComponent<Renderer>().material.color = Color.yellow;
        this.pos = pos;

        //copying components of Pick Up prefab
        Rigidbody rb = tempName.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        BoxCollider bc = tempName.AddComponent<BoxCollider>();
        Rotate rotator = tempName.AddComponent<Rotate>(); 
        bc.isTrigger = true;
        bc.size = new Vector3(1, 1, 1);

        this.name = name; //Changing name to Signal(1-8) by loop in SoundSources

    }


   /* void Start()
    {
        GameObject sg = GameObject.Find("Sound Sources"); //pulling stuff from other scripts MUST go into Start()
        SurfaceGenerator sgScript = sg.GetComponent<SurfaceGenerator>();
        radius = sgScript.floorSize * 4f;
        player = GameObject.Find("Player");
 
    */

    public string getName() //used for message.address in SoundSources.cs
    {
        return name;
    }
    //Just in case it is needed for OSC messages
    public float getAttenuation()
    {
        return attenuation;
    }

    public float getKymaConversion()
    {
        return kymaConversion;
    }

    public void updateValues(float radius)
    {
        GameObject sg = GameObject.Find("Sound Sources");
        SurfaceGenerator sgScript = sg.GetComponent<SurfaceGenerator>();
        player = GameObject.Find("Player");
        currentDistance = Vector3.Distance(pos, player.transform.position);

        attenuation = Mathf.Clamp(20 * Mathf.Log10(radius / currentDistance), -12f, 12f);

        // -12dB < attenuation < 12dB
        /*
        if (attenuation > 12)
        {
            attenuation = 12f;
        }
        else if (attenuation < -12)
        {
            attenuation = -12f;
        }*/
        kymaConversion = (attenuation + 12) / 24;
    }
}
//
//  PlayerController.cs
//
//  Author:
//       Kevin Cole <kevin.cole@novawebdevelopment.org>
//       Sophia Wallace <sophiawallace019@gmail.com>
//
//  Copyright (c) 2017 Kevin Cole CC-BY-SA (C) 2017
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;  // To access the on-screen text display
using System;          // DEBUG

public class PlayerController : MonoBehaviour {
    public  float     speed;            // Appears in the inspector
    public  Text      remainingText;    // UI text box
    public  Text      congratulations;  // Winner notification
    private Rigidbody rb;               // Affect the physics of the Player
    private int       collected;        // Number of collectables fetched

    public  OSC        oscReference;
    private OscMessage message;

    void Start() {
        rb = GetComponent<Rigidbody>();
        collected = 0;
        remainingText.text = "Cubes remaining: " + (8 - collected).ToString();
        congratulations.text = "";
    }

    // For physics / force updates?
    void FixedUpdate() {
        float   moveHorizontal = Input.GetAxis("Horizontal");
        float   moveVertical   = Input.GetAxis("Vertical");
        Vector3 movement       = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);


    }

    // On Collision, the "other" is what we collided with
    /*
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Pick Up")) {
            message = new OscMessage();
            message.address = "/vcs/SignalOn" + other.gameObject.name.Substring(6) + "/1";
            Console.WriteLine("{0}", other.gameObject.name.Substring(6));
            message.values.Add(0.0f);
            oscReference.Send(message);
            System.Threading.Thread.Sleep(50);
            other.gameObject.SetActive(false);
            collected++;
            remainingText.text = "Cubes remaining: " + (8 - collected).ToString();
        }
        

        if (collected == 8) {
            congratulations.text = "You win!";
        }
    }
    */
}
